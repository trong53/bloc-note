<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="BlocNote">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script&family=Lato&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./assets/css/homepage.css">
    <title>Bloc-Note</title>
</head>
<body>
    <div class="homepage">
        <header>
            <img src="./assets/Img/logo.png" alt="logo, sitedigital" class="logo">
            <div class="site-title">Bloc-Note</div>
        </header>

        <main>
            <div class="add-note-btn">
                <div class="add-note-icon">&#43;</div>
                <div class="add-note-text">Ajouter une note</div>
            </div>
            <div class="notes">
                <div class="note">Titre de la note 1</div>
                <div class="note">Titre de la note 2</div>
                <div class="note">Titre de la note 3</div>
                <div class="note">Titre de la note 4</div>
            </div>
            
        </main>
    </div>

</body>
</html>